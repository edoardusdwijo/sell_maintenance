<?php
include("../config.php");
session_start();
if (isset($_POST['login'])) {

    // ambil data dari formulir
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    // buat query
    $sql = "SELECT * FROM tbl_user WHERE nipp = '$username'";
    $query = mysqli_query($db, $sql);
    $data = mysqli_fetch_assoc($query);

    // apakah query simpan berhasil?
    if ($query) {
        if(mysqli_num_rows($query) == 0){
            echo "
		    		<script>
						alert('USERNAME / PASSWORD SALAH SILAHKAN LOGIN KEMBALI');
						document.location.href = 'index.php';
					</script>
		    	";
        }else if ($data['password'] == $password) {
            if ($data['role'] == 'super admin') {
                $_SESSION['roleAktif'] = "superAdmin";
                $_SESSION['nipp'] = $data['nipp'];
                echo "
		    			<script>
							alert('ANDA BERHASIL LOGIN SEBAGAI SUPER ADMIN');
                            document.location.href = '../super_admin/';
						</script>
		    			";
            } else if ($data['role'] == 'admin') {
                $_SESSION['roleAktif'] = "admin";
                $_SESSION['nipp'] = $data['nipp'];
                echo "
						<script>
		    			    alert('ANDA BERHASIL LOGIN SEBAGAI ADMIN');
							document.location.href = '../admin/';
						</script>
		    			";
            }
        } else {
            echo "
		    			<script>
							alert('USERNAME / PASSWORD SALAH SILAHKAN LOGIN KEMBALI');
							document.location.href = 'index.php';
						</script>
		    		";
        }
    } else {
        echo "
		    			<script>
							alert('USERNAME / PASSWORD SALAH SILAHKAN LOGIN KEMBALI');
							document.location.href = 'index.php';
						</script>
		    		";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>
</body>

</html>