<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/detaildataaset-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Detail Data Aset Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0) {
			?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">Data Aset Wesel-Detail Aset Wesel</p>
			<?php
			} else {
			?>
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
			<?php
			}
			?>
		</div>

		<?php
		$queryWesel = "SELECT * FROM `tbl_data_aset` WHERE noWesel='$noWesel'";
		$sqlWesel = mysqli_query($db, $queryWesel);
		if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0 && mysqli_num_rows($sqlWesel) != 0) {
			$dataWesel = mysqli_fetch_array($sqlWesel)
		?>
			<div class="card mb-4">
				<div class="card-header d-flex justify-content-between">
					<h4 class="wesel">WESEL <?php echo $dataWesel['noWesel'] ?></h4>
					<a class="edit" href="../edit-detail-data-aset/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" role="button">Edit <img src="../../src/icon/icon-edit.png" alt="" class="icon"></a>
				</div>
				<div class="card-body">
					<div class="d-flex">
						<div class="flex-shrink-0 order-2">
							<img src="../../src/img/<?php echo $dataWesel['fotoWesel'] ?>" alt="..." class="gambar">
						</div>
						<div class="flex-grow-1 ms-3">
							<div class="posisi d-flex justify-content-between">
								<p>Posisi Ujung Wesel</p>
								<p class="kotak"><?php echo $dataWesel['posisiUjung'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Posisi Pangkal Wesel</p>
								<p class="kotak"><?php echo $dataWesel['posisiPangkal'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Sudut Wesel</p>
								<p class="kotak"><?php echo $dataWesel['sudutWesel'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Merk Wesel</p>
								<p class="merk"><?php echo $dataWesel['merk'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Arah Wesel</p>
								<p class="kotak"><?php echo $dataWesel['arah'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Tipe Rel</p>
								<p class="kotak"><?php echo $dataWesel['tipeRel'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Lidah</p>
								<p class="kotak"><?php echo $dataWesel['lidah'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Terlayan</p>
								<p class="kotak"><?php echo $dataWesel['terlayan'] ?></p>
							</div>
							<div class="posisi d-flex justify-content-between">
								<p>Tahun Produksi</p>
								<p class="kotak"><?php echo $dataWesel['tahun'] ?></p>
							</div>
						</div>
					</div>
					<div class="row text-center">
						<div class="col-sm-4">
							<?php
							$queryTotalRawat = "SELECT * FROM `tbl_pemeriksaan` WHERE year(tanggal)=" . date('Y') . " AND `status`='selesai' AND noWesel='" . $noWesel . "'";
							$sqlTotalRawat = mysqli_query($db, $queryTotalRawat);
							?>
							<p>Total Perawatan Tahun Ini <button type="button" class="btn btn-primary perawatan" disabled><?php echo mysqli_num_rows($sqlTotalRawat) ?></button></p>
						</div>
						<div class="col-sm-4">
							<?php
							if ($dataWesel['jenisJalur'] == 'KA') {
								if (mysqli_num_rows($sqlTotalRawat) == 2) {
							?>
									<button type='button' class='btn btn-primary pemeriksaan' data-bs-toggle='modal' data-bs-target='#disable1'>Pemeriksaan Wesel <img src="../../src/icon/icon-pemeriksaan.png" alt=""></button>
									<div class='modal fade' id='disable1' tabindex='-1' aria-labelledby='disableLabel' aria-hidden='true'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header'>
													<h5 class='modal-title' id='kembaliLabel'>Peringatan!!</h5>
													<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
												</div>
												<div class='modal-body'>
													DATA ASET WESEL INI BERADA DI JALUR KA DAN PEMERIKSAAN / PERAWATAN HANYA DI BATASI HINGGA 2 KALI DALAM SETAHUN, TUNGGU HINGGA TAHUN SELANJUTNYA SAMPAI PEMERIKSAAN WESEL INI DIBUKA KEMBALI
												</div>
												<div class='modal-footer'>
													<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Close</button>
												</div>
											</div>
										</div>
									</div>
								<?php
								} else {
								?>
									<a class="btn btn-primary pemeriksaan" href="../pemeriksaan/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $dataWesel['noWesel'] ?>" role="button">Pemeriksaan Wesel <img src="../../src/icon/icon-pemeriksaan.png" alt=""></a>
								<?php
								}
							} else {
								if (mysqli_num_rows($sqlTotalRawat) == 4) {
								?>
									<button type='button' class='btn btn-primary pemeriksaan' data-bs-toggle='modal' data-bs-target='#disable2'>Pemeriksaan Wesel <img src="../../src/icon/icon-pemeriksaan.png" alt=""></button>
									<div class='modal fade' id='disable2' tabindex='-1' aria-labelledby='disableLabel' aria-hidden='true'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header'>
													<h5 class='modal-title' id='kembaliLabel'>Peringatan!!</h5>
													<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
												</div>
												<div class='modal-body'>
													DATA ASET WESEL INI BERADA DI JALUR RAYA DAN PEMERIKSAAN / PERAWATAN HANYA DI BATASI HINGGA 4 KALI DALAM SETAHUN, TUNGGU HINGGA TAHUN SELANJUTNYA SAMPAI PEMERIKSAAN WESEL INI DIBUKA KEMBALI
												</div>
												<div class='modal-footer'>
													<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Close</button>
												</div>
											</div>
										</div>
									</div>
								<?php
								} else {
								?>
									<a class="btn btn-primary pemeriksaan" href="../pemeriksaan/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $dataWesel['noWesel'] ?>" role="button">Pemeriksaan Wesel <img src="../../src/icon/icon-pemeriksaan.png" alt=""></a>
							<?php
								}
							}
							?>
						</div>
						<div class="col-sm-4">
							<a class="btn btn-primary pemeriksaan" href="../perawatan/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $dataWesel['noWesel'] ?>" role="button">Perawatan Wesel&nbsp;&nbsp;<img src="../../src/icon/icon-perawatan.png" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		<?php
		} else {
		?>
			<div class="peringatan text-center">
				<p>Tidak Ada Data Aset Wesel</p>
			</div>
		<?php
		}
		?>

		<div class="tabel table-responsive mt-4 mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th scope="col" width="5%" class="text-center">No</th>
						<th scope="col" class="text-center">Tanggal</th>
						<th scope="col" class="text-center">Preview Document</th>
						<th scope="col" class="text-center">Cetak Document</th>
						<th scope="col" class="text-center">Upload Document</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = "SELECT * FROM tbl_pemeriksaan WHERE noWesel = '$noWesel' AND `status`='selesai' ORDER BY tanggal DESC";
					$sql = mysqli_query($db, $query);
					$no = 1;
					while ($data = mysqli_fetch_array($sql)) {
					?>
						<tr>
							<td class='text-center'><?php echo $no ?></td>
							<td class='text-center'><?php echo $data['tanggal'] ?></td>
							<td class='text-center'>
								<a class="btn btn-primary btn-sm" href="../preview-perawatan/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>&id=<?php echo $data['id'] ?>" role="button">Preview</a>
							</td>
							<td class='text-center'>
								<a class="btn btn-primary btn-sm" href="../cetak/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>&id=<?php echo $data['id'] ?>" role="button" target="_blank">Cetak / Download</a>
							</td>
							<td class='d-flex justify-content-center'>
								<?php
									$queryCekUpload = "SELECT * FROM doc_perawatan WHERE id='".$data['id']."'";
									$sqlCekUpload = mysqli_query($db,$queryCekUpload);
									$cek = mysqli_num_rows($sqlCekUpload);
									if($cek == 0){
										?>
											<button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#uploadDoc<?php echo $no ?>">Upload</button>
										<?php
									}else{
										?>
										<div class="tombolUpload">
											<button type="button" class="btn btn-success btn-sm upload" style="--bs-btn-padding-x: 1.7rem;" disabled><img src="../../src/icon/icon-centang.png" alt=""></button>
										</div>
										<?php
									}
								?>

								<div class="modal fade" id="uploadDoc<?php echo $no ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<form action="confDetailDataAset.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>&id=<?php echo $data['id'] ?>" enctype="multipart/form-data" method="POST">
											<div class="modal-content">
												<div class="modal-header">
													<h1 class="modal-title fs-5" id="exampleModalLabel">Upload Document</h1>
													<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
												</div>
												<div class="modal-body">
													<div class="mb-3">
														<label for="formFile" class="form-label">Upload Document (.pdf)</label>
														<input class="form-control" type="file" id="formFile" name="doc" require>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
													<button type="submit" class="btn btn-primary" name="submit" value="submit">Upload</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</td>
						</tr>
					<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>