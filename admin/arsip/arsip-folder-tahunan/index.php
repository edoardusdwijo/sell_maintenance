<?php
include("../../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../../login");
	exit;
}

$resort = $_GET['resort'];
$emplasemen = $_GET['emplasemen'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/arsipfoldertahunan-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Folder Tahunan - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../../"><img src="../../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../../data-aset/"><img src="../../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../" class="active"><img src="../../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../../status-reporting/"><img src="../../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../../about/"><img src="../../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../../logout.php"><img src="../../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<p class="resor">ARSIP <?php echo $resort ?></p>
			<p class="emplasemen"><?php echo $emplasemen ?></p>
		</div>

		<?php
		$queryDataTahun = "SELECT year(tanggal) AS tahun FROM doc_perawatan WHERE resort='$resort' AND emplasemen='$emplasemen'";
		$sqlDataTahun = mysqli_query($db, $queryDataTahun);
		if (mysqli_num_rows($sqlDataTahun) == 0) {
		?>
			<div class="peringatan">
				<p>TIDAK ADA DATA PERAWATAN</p>
			</div>
		<?php
		} else {
		?>
			<div class="row row-cols-1 row-cols-md-2 g-4 mt-5 mb-4 folder">
				<?php
				$tempTahun = "";
				while ($dataTahun = mysqli_fetch_array($sqlDataTahun)) {
					if ($tempTahun != $dataTahun['tahun']) {
				?>
						<div class="col">
							<div class="card">
								<a href="../arsip-pilih-jalur/?resort=<?php echo $resort ?>&emplasemen=<?php echo $emplasemen ?>&tahun=<?php echo $dataTahun['tahun'] ?>">
									<img src="../../../src/icon/icon-folder-tahun.png" class="card-img-top" alt="...">
									<div class="card-body">
										<h5 class="card-title"><?php echo $dataTahun['tahun'] ?></h5>
									</div>
								</a>
							</div>
						</div>
				<?php
					}
				$tempTahun = $dataTahun['tahun'];
				}
				?>
			</div>
		<?php
		}
		?>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>