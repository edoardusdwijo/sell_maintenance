<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];
$id = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/cetak-admin.css" media="screen">
	<link rel="stylesheet" href="../../css/cetak-admin1.css" media="print">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Cetak Document - Admin</title>
</head>

<body>
	<!-- start content -->
	<div class="content">
		<div class="header d-flex justify-content-between">
			<div class="tanggal">
				<?php
					$sqlTgl = "SELECT * FROM tbl_pemeriksaan WHERE id='$id'";
					$queryTgl = mysqli_query($db, $sqlTgl);
					$dataTgl = mysqli_fetch_array($queryTgl);
				?>
				<p><?php echo $dataTgl['tanggal'] ?></p>
			</div>

			<div class="nama text-end">
				<p>SELL <span>Maintenance</span></p>
			</div>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			$queryWesel = "SELECT * FROM `tbl_data_aset` WHERE noWesel='$noWesel'";
			$sqlWesel = mysqli_query($db, $queryWesel);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0 && mysqli_num_rows($sqlWesel) != 0) {
				?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">WESEL <?php echo $noWesel ?>-Dokumen Hasil Pemeriksaan dan Perawatan Wesel</p>
				<?php
			} else {
				?>
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
				<?php
			}
			?>
		</div>
		<?php
		$queryPreview = "SELECT * FROM tbl_pemeriksaan, tbl_pemeriksaan_ukt, tbl_pemeriksaan_ls, tbl_pemeriksaan_op WHERE tbl_pemeriksaan.id='$id' AND tbl_pemeriksaan_ukt.id='$id' AND tbl_pemeriksaan_ls.id='$id' AND tbl_pemeriksaan_op.id='$id'";
		$sqlPreview = mysqli_query($db, $queryPreview);
		$data = mysqli_fetch_array($sqlPreview);
		?>
		<div class="tabelPreview table-responsive mb-4">
			<table class="table table-bordered table-light rounded-3 overflow-hidden" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th colspan="6" class="text-center" width="40%">Ukuran Tempat-Tempat Penting</th>
						<th colspan="6" class="text-center">Lebar Sepur</th>
						<th colspan="2" rowspan="2" class="text-center align-middle">Opname Lain-Lain</th>
					</tr>
					<tr>
						<th colspan="3" class="text-center">Lurus</th>
						<th colspan="3" class="text-center">Belok</th>
						<th colspan="3" class="text-center">Lurus</th>
						<th colspan="3" class="text-center">Belok</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="6" rowspan="4">
							<center>Jarak</center>
						</td>
						<td colspan="6">
							<center>Lebar Sepur 6M di belakang Jarum</center>
						</td>
						<td colspan="2" width="25%">Kondisi Jarum</td>
					</tr>
					<tr class="align-middle">
						<!-- <td></td> -->
						<td><?php echo $data['ls_ls6m_jarumLurus_nStandar'] ?></td>
						<td><?php echo $data['ls_ls6m_jarumLurus_nPemeriksaan'] ?></td>
						<td><?php echo $data['ls_ls6m_jarumLurus_nPerawatan'] ?></td>
						<td><?php echo $data['ls_ls6m_jarumBelok_nStandar'] ?></td>
						<td><?php echo $data['ls_ls6m_jarumBelok_nPemeriksaan'] ?></td>
						<td><?php echo $data['ls_ls6m_jarumBelok_nPerawatan'] ?></td>
						<td class="text-break"><?php echo $data['op_kondisiJarum'] ?></td>
					</tr>
					<tr class="align-middle">
						<!-- <td></td> -->
						<td colspan="3">-2<br>+5</td>
						<td colspan="3">-2<br>+5</td>
						<td colspan="2" width="25%">Kondisi Jarum Setelah Perawatan</td>
					</tr>
					<tr class="align-middle">
						<!-- <td></td> -->
						<td colspan="6">
							<center>Lebar Sepur tepat dibelakang Jarum</center>
						</td>
						<td class="text-break"><?php echo $data['op_hasilJarum'] ?></td>
					</tr>
					<tr class="align-middle">
						<td colspan="6">
							<center>Lebar alur pada jarum dengan rel paksa</center>
						</td>
						<td><?php echo $data['ls_lst_jarumLurus_nStandar'] ?></td>
						<td><?php echo $data['ls_lst_jarumLurus_nPemeriksaan'] ?></td>
						<td><?php echo $data['ls_lst_jarumLurus_nPerawatan'] ?></td>
						<td><?php echo $data['ls_lst_jarumBelok_nStandar'] ?></td>
						<td><?php echo $data['ls_lst_jarumBelok_nPemeriksaan'] ?></td>
						<td><?php echo $data['ls_lst_jarumBelok_nPerawatan'] ?></td>
						<td colspan="2" width="25%">Kondisi Vangrel</td>
					</tr>
					<tr>
						<td width="7%"><?php echo $data['ukt_laj_paksaLurus_nStandar'] ?></td>
						<td width="7%"><?php echo $data['ukt_laj_paksaLurus_nPemeriksaan'] ?></td>
						<td width="7%"><?php echo $data['ukt_laj_paksaLurus_nPerawatan'] ?></td>
						<td><?php echo $data['ukt_laj_paksaBelok_nStandar'] ?></td>
						<td><?php echo $data['ukt_laj_paksaBelok_nPemeriksaan'] ?></td>
						<td><?php echo $data['ukt_laj_paksaBelok_nPerawatan'] ?></td>
						<td colspan="6" rowspan="2">
							<center>Lebar alur rel paksa bagian belakang</center>
						</td>
						<!-- <td>1067</td>
						<td>1032</td>
						<td>0</td>
						<td>1067</td>
						<td>1032</td>
						<td>0</td> -->
						<td class="text-break"><?php echo $data['op_kondisiVangrel'] ?></td>
					</tr>
					<tr>
						<td colspan="3" rowspan="2">+2<br>-0</td>
						<td colspan="3" rowspan="2">+2<br>-0</td>
						<!-- <td colspan="6"></td> -->
						<td colspan="2" width="25%">Kondisi Vangrel Setelah Perawatan</td>
					</tr>
					<tr class="align-middle">
						<td><?php echo $data['ls_la_paksaLurus_nStandar1'] ?></td>
						<td><?php echo $data['ls_la_paksaLurus_nPemeriksaan1'] ?></td>
						<td><?php echo $data['ls_la_paksaLurus_nPerawatan1'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nStandar1'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nPemeriksaan1'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nPerawatan1'] ?></td>
						<td class="text-break"><?php echo $data['op_hasilVangrel'] ?></td>
					</tr>
					<tr>
						<!-- <td></td> -->
						<td colspan="6">
							<center>Lebar alur rel paksa</center>
						</td>
						<td><?php echo $data['ls_la_paksaLurus_nStandar2'] ?></td>
						<td><?php echo $data['ls_la_paksaLurus_nPemeriksaan2'] ?></td>
						<td><?php echo $data['ls_la_paksaLurus_nPerawatan2'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nStandar2'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nPemeriksaan2'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nPerawatan2'] ?></td>
						<td colspan="2" width="25%">Kondisi Lidah</td>
					</tr>
					<tr class="align-middle">
						<td><?php echo $data['ukt_la_paksaLurus_nStandar'] ?></td>
						<td><?php echo $data['ukt_la_paksaLurus_nPemeriksaan'] ?></td>
						<td><?php echo $data['ukt_la_paksaLurus_nPerawatan'] ?></td>
						<td><?php echo $data['ukt_la_paksaBelok_nStandar'] ?></td>
						<td><?php echo $data['ukt_la_paksaBelok_nPemeriksaan'] ?></td>
						<td><?php echo $data['ukt_la_paksaBelok_nPerawatan'] ?></td>
						<td colspan="3">+2<br>-2</td>
						<td colspan="3">+2<br>-2</td>
						<td class="text-break"><?php echo $data['op_kondisiLidah'] ?></td>
					</tr>
					<tr>
						<td colspan="6" rowspan="2"></td>
						<td colspan="6">
							<center>Lebar alur rel paksa bagian depan</center>
						</td>
						<td colspan="2" width="25%">Kondisi Lidah Setelah Perawatan</td>
					</tr>
					<tr>
						<td><?php echo $data['ls_la_paksaLurus_nStandar3'] ?></td>
						<td><?php echo $data['ls_la_paksaLurus_nPemeriksaan3'] ?></td>
						<td><?php echo $data['ls_la_paksaLurus_nPerawatan3'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nStandar3'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nPemeriksaan3'] ?></td>
						<td><?php echo $data['ls_la_paksaBelok_nPerawatan3'] ?></td>
						<td class="text-break"><?php echo $data['op_hasilLidah'] ?></td>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<center>Jarak antara pangkal lidah dan rel lantak</center>
					</td>
					<td><?php echo $data['ls_la_paksaLurus_nStandar4'] ?></td>
					<td><?php echo $data['ls_la_paksaLurus_nPemeriksaan4'] ?></td>
					<td><?php echo $data['ls_la_paksaLurus_nPerawatan4'] ?></td>
					<td><?php echo $data['ls_la_paksaBelok_nStandar4'] ?></td>
					<td><?php echo $data['ls_la_paksaBelok_nPemeriksaan4'] ?></td>
					<td><?php echo $data['ls_la_paksaBelok_nPerawatan4'] ?></td>
					<td colspan="2" width="25%">Kondisi Rel Lantak</td>
				</tr>
				<tr class="align-middle">
					<td><?php echo $data['ukt_japl_lantakLurus_nStandar'] ?></td>
					<td><?php echo $data['ukt_japl_lantakLurus_nPemeriksaan'] ?></td>
					<td><?php echo $data['ukt_japl_lantakLurus_nPerawatan'] ?></td>
					<td><?php echo $data['ukt_japl_lantakBelok_nStandar'] ?></td>
					<td><?php echo $data['ukt_japl_lantakBelok_nPemeriksaan'] ?></td>
					<td><?php echo $data['ukt_japl_lantakBelok_nPerawatan'] ?></td>
					<td colspan="3">+2<br>-2</td>
					<td colspan="3">+2<br>-2</td>
					<td class="text-break"><?php echo $data['op_kondisiRelLantak'] ?></td>
				</tr>
				<tr>
					<td colspan="3" rowspan="2"></td>
					<td colspan="3" rowspan="2"></td>
					<td colspan="6">
						<center>Lebar Sepur pada titik matematis</center>
					</td>
					<td colspan="2" width="25%">Kondisi Rel Lantak Setelah Perawatan</td>
				</tr>
				<tr class="align-middle">
					<td><?php echo $data['ls_ls_tMatematisLurus_nStandar'] ?></td>
					<td><?php echo $data['ls_ls_tMatematisLurus_nPemeriksaan'] ?></td>
					<td><?php echo $data['ls_ls_tMatematisLurus_nPerawatan'] ?></td>
					<td><?php echo $data['ls_ls_tMatematisBelok_nStandar'] ?></td>
					<td><?php echo $data['ls_ls_tMatematisBelok_nPemeriksaan'] ?></td>
					<td><?php echo $data['ls_ls_tMatematisBelok_nPerawatan'] ?></td>
					<td class="text-break"><?php echo $data['op_hasilRelLantak'] ?></td>
				</tr>
				<tr>
					<td colspan="3">
						<center>Connecting Rod</center>
					</td>
					<td colspan="3"></td>
					<td colspan="6">
						<center>Lebar Sepur pada pangkal lidah</center>
					</td>
					<td colspan="2" width="25%">Kondisi Bantalan</td>
				</tr>
				<tr class="align-middle">
					<td><?php echo $data['ukt_connectingRod_nStandar'] ?></td>
					<td><?php echo $data['ukt_connectingRod_nPemeriksaan'] ?></td>
					<td><?php echo $data['ukt_connectingRod_nPerawatan'] ?></td>
					<td colspan="3"></td>
					<td><?php echo $data['ls_ls_pLidahLurus_nStandar'] ?></td>
					<td><?php echo $data['ls_ls_pLidahLurus_nPemeriksaan'] ?></td>
					<td><?php echo $data['ls_ls_pLidahLurus_nPerawatan'] ?></td>
					<td><?php echo $data['ls_ls_pLidahBelok_nStandar'] ?></td>
					<td><?php echo $data['ls_ls_pLidahBelok_nPemeriksaan'] ?></td>
					<td><?php echo $data['ls_ls_pLidahBelok_nPerawatan'] ?></td>
					<td colspan="text-break"><?php echo $data['op_kondisiBantalan'] ?></td>
				</tr>
				<tr>
					<td colspan="6" rowspan="2">
						<center>Jarak antara ujung lidah terbuka degan rel lantak</center>
					</td>
					<td colspan="3">-2<br>+5</td>
					<td colspan="3">-2<br>+5</td>
					<td colspan="2" width="25%">Kondisi Batalan Setelah Perawatan</td>
				</tr>
				<tr>
					<td colspan="6">
						<center>Lebar Sepur pada lidah</center>
					</td>
					<td><?php echo $data['op_hasilBantalan'] ?></td>
				</tr>
				<tr class="align-middle">
					<td><?php echo $data['ukt_jault_lantakLurus_nStandar'] ?></td>
					<td><?php echo $data['ukt_jault_lantakLurus_nPemeriksaan'] ?></td>
					<td><?php echo $data['ukt_jault_lantakLurus_nPerawatan'] ?></td>
					<td><?php echo $data['ukt_jault_lantakBelok_nStandar'] ?></td>
					<td><?php echo $data['ukt_jault_lantakBelok_nPemeriksaan'] ?></td>
					<td><?php echo $data['ukt_jault_lantakBelok_nPerawatan'] ?></td>
					<td><?php echo $data['ls_ls_ppLidahLurus_nStandar'] ?></td>
					<td><?php echo $data['ls_ls_ppLidahLurus_nPemeriksaan'] ?></td>
					<td><?php echo $data['ls_ls_ppLidahLurus_nPerawatan'] ?></td>
					<td><?php echo $data['ls_ls_ppLidahBelok_nStandar'] ?></td>
					<td><?php echo $data['ls_ls_ppLidahBelok_nPemeriksaan'] ?></td>
					<td><?php echo $data['ls_ls_ppLidahBelok_nPerawatan'] ?></td>
					<td colspan="2" width="25%">Kondisi Penambat</td>
				</tr>
				<tr>
					<td colspan="3" rowspan="2"></td>
					<td colspan="3"></td>
					<!-- <td colspan="3" rowspan="3"></td> -->
					<td colspan="6">
						<center>Lebar Sepur Bagian Rel Lantak</center>
					</td>
					<td class="text-break"><?php echo $data['op_kondisiPenambat'] ?></td>
				</tr>
				<tr class="align-middle">
					<td><?php echo $data['ukt_tss_lantak_nStandar'] ?></td>
					<td><?php echo $data['ukt_tss_lantak_nPemeriksaan'] ?></td>
					<td><?php echo $data['ukt_tss_lantak_nPerawatan'] ?></td>
					<td colspan="3"></td>
					
					<td><?php echo $data['ls_ls_lantak_nStandar'] ?></td>
					<td><?php echo $data['ls_ls_lantak_nPemeriksaan'] ?></td>
					<td><?php echo $data['ls_ls_lantak_nPerawatan'] ?></td>
					<td colspan="2" width="25%">Kondisi Penambat Setelah Perawatan</td>
				</tr>
				<tr>
					<td colspan="6">
						<center>Tidak sikunya sambungan pada rel lantak</center>
					</td>
					<td colspan="6">
						<center>-2<br>+5</center>
					</td>
					<td class="text-break"><?php echo $data['op_hasilPenambat'] ?></td>
				</tr>
			</tbody>
		</table>

		<div class="row g-4 mt-4 mb-4 w-100">
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPemeriksaan/<?php echo $data['op_kondisiJarum_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Jarum</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPerawatan/<?php echo $data['op_hasilJarum_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Jarum Setelah Perawatan</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPemeriksaan/<?php echo $data['op_kondisiVangrel_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Vangrel</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPerawatan/<?php echo $data['op_hasilVangrel_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Vangrel Setelah Perawatan</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPemeriksaan/<?php echo $data['op_kondisiLidah_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Lidah</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPerawatan/<?php echo $data['op_hasilLidah_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Lidah Setelah Perawatan</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPemeriksaan/<?php echo $data['op_kondisiRelLantak_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Rel Lantak</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPerawatan/<?php echo $data['op_hasilRelLantak_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Rel Lantak Setelah Perawatan</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPemeriksaan/<?php echo $data['op_kondisiBantalan_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Bantalan</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPerawatan/<?php echo $data['op_hasilBantalan_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Bantalan Setelah Perawatan</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPemeriksaan/<?php echo $data['op_kondisiPenambat_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Penambat</h5>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<img src="../../src/gambarPerawatan/<?php echo $data['op_hasilPenambat_foto'] ?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Foto Kondisi Penambat Setelah Perawatan</h5>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

<script>
	window.print();
</script>

</html>