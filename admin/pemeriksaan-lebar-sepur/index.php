<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}

$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (!isset($_SESSION['id' . $noWesel])) {
	header("location:../data-aset/");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/pemeriksaanlebarsepur-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Pemeriksaan Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			$queryWesel = "SELECT * FROM `tbl_data_aset` WHERE noWesel='$noWesel'";
			$sqlWesel = mysqli_query($db, $queryWesel);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0 && mysqli_num_rows($sqlWesel) != 0) {
			?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">WESEL <?php echo $noWesel ?>-Pemeriksaan Wesel</p>
			<?php
			} else {
			?>
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
			<?php
			}
			?>
		</div>
		<?php
		$queryCheck = "SELECT * FROM `tbl_pemeriksaan_ls` WHERE id='" . $_SESSION['id' . $noWesel] . "'";
		$sqlCheck = mysqli_query($db, $queryCheck);
		if (mysqli_num_rows($sqlCheck) == 0) {
		?>
			<div class="judul-form">
				<p>Lebar Sepur</p>
			</div>
			<form action="confPemeriksaanLebarSepur.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" method="POST">
				<div class="pemeriksaan">
					<div class="card w-100 mb-4">
						<div class="card-body">
							<div class="row g-3">
								<p style="margin-bottom: 0px;">Lebar Sepur 6 M dibelakang Jarum Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls6m_jarumLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls6m_jarumLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls6m_jarumLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur 6 M dibelakang Jarum Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls6m_jarumBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls6m_jarumBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls6m_jarumBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur Tepat dibelakang Jarum Lurus</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_lst_jarumLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_lst_jarumLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_lst_jarumLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur Tepat dibelakang Jarum Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_lst_jarumBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_lst_jarumBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_lst_jarumBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Alur Rel Paksa Lurus</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar  (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar1" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan1" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan1" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar2" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan2" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan2" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar3" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan3" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan3" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar4" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan4" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan4" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Alur Rel Paksa Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar1" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan1" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan1" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar2" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan2" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan2" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar3" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan3" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan3" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar4" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan4" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan4" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Titik Matematis Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_tMatematisLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_tMatematisLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_tMatematisLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Titik Matematis Belok</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_tMatematisBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_tMatematisBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_tMatematisBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Pangkal Lidah Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_ppLidahLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_ppLidahLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_ppLidahLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Pangkal Lidah Belok</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_ppLidahBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_ppLidahBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_ppLidahBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Lidah Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_pLidahLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_pLidahLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_pLidahLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Lidah Belok</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_pLidahBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_pLidahBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_pLidahBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur Bagian Lantak</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_lantak_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_lantak_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_lantak_nPerawatan" disabled>
								</div>
							</div>
						</div>
					</div>
					<div class="tombol d-flex justify-content-between mb-4">
						<button type='button' class='btn btn-primary btn-sm' data-bs-toggle='modal' data-bs-target='#kembali'>Halaman Sebelumnya</button>
						<button class="btn btn-primary btn-sm" type="submit" value="next" name="next">Halaman Selanjutnya</button>
					</div>
					<div class='modal fade' id='kembali' tabindex='-1' aria-labelledby='kembaliLabel' aria-hidden='true'>
						<div class='modal-dialog'>
							<div class='modal-content'>
								<div class='modal-header'>
									<h5 class='modal-title' id='kembaliLabel'>Peringatan!!</h5>
									<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
								</div>
								<div class='modal-body'>
									Apakah anda yakin untuk kembali ke halaman sebelumnya? progress anda di halaman ini tidak akan tersimpan
								</div>
								<div class='modal-footer'>
									<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
									<a class="btn btn-primary" href="../pemeriksaan/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" role="button">Halaman Sebelumnya</a>
								</div>
							</div>
						</div>
					</div>
			</form>
		<?php
		} else if (mysqli_num_rows($sqlCheck) != 0 && $dataPemeriksaanLs = mysqli_fetch_array($sqlCheck)) {
		?>
			<div class="judul-form">
				<p>Lebar Sepur</p>
			</div>
			<form action="confPemeriksaanLebarSepur.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" method="POST">
				<div class="pemeriksaan">
					<div class="card w-100 mb-4">
						<div class="card-body">
							<div class="row g-3">
								<p style="margin-bottom: 0px;">Lebar Sepur 6 M dibelakang Jarum Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls6m_jarumLurus_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls6m_jarumLurus_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls6m_jarumLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls6m_jarumLurus_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls6m_jarumLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur 6 M dibelakang Jarum Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls6m_jarumBelok_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls6m_jarumBelok_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls6m_jarumBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls6m_jarumBelok_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls6m_jarumBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur Tepat dibelakang Jarum Lurus</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_lst_jarumLurus_nStandar" value="<?php echo $dataPemeriksaanLs['ls_lst_jarumLurus_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_lst_jarumLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_lst_jarumLurus_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_lst_jarumLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur Tepat dibelakang Jarum Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_lst_jarumBelok_nStandar" value="<?php echo $dataPemeriksaanLs['ls_lst_jarumBelok_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_lst_jarumBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_lst_jarumBelok_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_lst_jarumBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Alur Rel Paksa Lurus</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar1" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nStandar1'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan1" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nPemeriksaan1'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan1" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar2" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nStandar2'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan2" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nPemeriksaan2'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Belakang)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan2" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar3" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nStandar3'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan3" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nPemeriksaan3'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan3" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaLurus_nStandar4" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nStandar4'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaLurus_nPemeriksaan4" value="<?php echo $dataPemeriksaanLs['ls_la_paksaLurus_nPemeriksaan4'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan (Depan)</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaLurus_nPerawatan4" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Alur Rel Paksa Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar1" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nStandar1'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan1" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nPemeriksaan1'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan1" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar2" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nStandar2'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan2" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nPemeriksaan2'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan2" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar3" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nStandar3'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan3" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nPemeriksaan3'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan3" disabled>
								</div>

								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_la_paksaBelok_nStandar4" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nStandar4'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_la_paksaBelok_nPemeriksaan4" value="<?php echo $dataPemeriksaanLs['ls_la_paksaBelok_nPemeriksaan4'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_la_paksaBelok_nPerawatan4" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Titik Matematis Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_tMatematisLurus_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls_tMatematisLurus_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_tMatematisLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls_tMatematisLurus_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_tMatematisLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Titik Matematis Belok</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_tMatematisBelok_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls_tMatematisBelok_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_tMatematisBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls_tMatematisBelok_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_tMatematisBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Pangkal Lidah Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_ppLidahLurus_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls_ppLidahLurus_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_ppLidahLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls_ppLidahLurus_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_ppLidahLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Pangkal Lidah Belok</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_ppLidahBelok_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls_ppLidahBelok_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_ppLidahBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls_ppLidahBelok_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_ppLidahBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Lidah Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_pLidahLurus_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls_pLidahLurus_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_pLidahLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls_pLidahLurus_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_pLidahLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur pada Lidah Belok</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_pLidahBelok_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls_pLidahBelok_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_pLidahBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls_pLidahBelok_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_pLidahBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Sepur Bagian Lantak</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ls_ls_lantak_nStandar" value="<?php echo $dataPemeriksaanLs['ls_ls_lantak_nStandar'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ls_ls_lantak_nPemeriksaan" value="<?php echo $dataPemeriksaanLs['ls_ls_lantak_nPemeriksaan'] ?>" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ls_ls_lantak_nPerawatan" disabled>
								</div>
							</div>
						</div>
					</div>
					<div class="tombol d-flex justify-content-between mb-4">
						<a class="btn btn-primary btn-sm" href="../pemeriksaan/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" role="button">Halaman Sebelumnya</a>
						<button class="btn btn-primary btn-sm" type="submit" value="edit" name="edit">Halaman Selanjutnya</button>
					</div>
			</form>
		<?php
		}
		?>

	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>