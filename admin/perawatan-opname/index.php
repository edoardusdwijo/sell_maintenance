<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];
$id = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/perawatanopname-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Perawatan Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			$queryWesel = "SELECT * FROM `tbl_data_aset` WHERE noWesel='$noWesel'";
			$sqlWesel = mysqli_query($db, $queryWesel);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0 && mysqli_num_rows($sqlWesel) != 0) {
			?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">WESEL <?php echo $noWesel ?>-Perawatan Wesel</p>
			<?php
			} else {
			?>
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
			<?php
			}
			?>
		</div>

		<div class="judul-form">
			<p>Hasil Opname</p>
		</div>

		<form action="confPerawatanOpname.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" enctype="multipart/form-data" method="POST">
			<div class="pemeriksaan">
				<div class="card w-100 mb-4">
					<div class="card-body">
						<div class="row g-3">
							<div class="col-md-4">
								<label for="inputEmail4" class="form-label">Hasil Opname Jarum</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Keterangan Opname Jarum" name="op_hasilJarum" required>
								<input type="hidden" class="form-control" name="id" value="<?php echo $id ?>">
							</div>
							<div class="col-md-8">
								<label for="inputEmail4" class="form-label">Foto Kondisi Jarum ukuran max 5MB</label>
								<input class="form-control" type="file" id="fotoWesel" name="op_hasilJarum_foto" required>
							</div>

							<div class="col-md-4">
								<label for="inputEmail4" class="form-label">Hasil Opname Vangrel</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Keterangan Opname Vangrel" name="op_hasilVangrel" required>
							</div>
							<div class="col-md-8">
								<label for="inputEmail4" class="form-label">Foto Kondisi Vangrel ukuran max 5MB</label>
								<input class="form-control" type="file" id="fotoWesel" name="op_hasilVangrel_foto" required>
							</div>

							<div class="col-md-4">
								<label for="inputEmail4" class="form-label">Hasil Opname Lidah</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Keterangan Opname Lidah" name="op_hasilLidah" required>
							</div>
							<div class="col-md-8">
								<label for="inputEmail4" class="form-label">Foto Kondisi Lidah ukuran max 5MB</label>
								<input class="form-control" type="file" id="fotoWesel" name="op_hasilLidah_foto" required>
							</div>

							<div class="col-md-4">
								<label for="inputEmail4" class="form-label">Hasil Opname Rel Lantak</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Keterangan Opname Rel Lantak" name="op_hasilRelLantak" required>
							</div>
							<div class="col-md-8">
								<label for="inputEmail4" class="form-label">Foto Kondisi Rel Lantak ukuran max 5MB</label>
								<input class="form-control" type="file" id="fotoWesel" name="op_hasilRelLantak_foto" required>
							</div>

							<div class="col-md-4">
								<label for="inputEmail4" class="form-label">Hasil Opname Bantalan</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Keterangan Opname Bantalan" name="op_hasilBantalan" required>
							</div>
							<div class="col-md-8">
								<label for="inputEmail4" class="form-label">Foto Kondisi Bantalan ukuran max 5MB</label>
								<input class="form-control" type="file" id="fotoWesel" name="op_hasilBantalan_foto" required>
							</div>

							<div class="col-md-4">
								<label for="inputEmail4" class="form-label">Hasil Opname Penambat</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Keterangan Opname Penambat" name="op_hasilPenambat" required>
							</div>
							<div class="col-md-8">
								<label for="inputEmail4" class="form-label">Foto Kondisi Penambat ukuran max 5MB</label>
								<input class="form-control" type="file" id="fotoWesel" name="op_hasilPenambat_foto" required>
							</div>
						</div>
					</div>
				</div>
				<div class="tombol d-flex justify-content-between mb-4">
					<button type='button' class='btn btn-primary btn-sm' data-bs-toggle='modal' data-bs-target='#kembali'>Halaman Sebelumnya</button>
					<button type='button' class='btn btn-primary btn-sm' data-bs-toggle='modal' data-bs-target='#simpanData'>Simpan</button>
				</div>
				<div class='modal fade' id='simpanData' tabindex='-1' aria-labelledby='simpanDataLabel' aria-hidden='true'>
					<div class='modal-dialog'>
						<div class='modal-content'>
							<div class='modal-header'>
								<h5 class='modal-title' id='simpanDataLabel'>Peringatan!!</h5>
								<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
							</div>
							<div class='modal-body'>
								Apakah anda yakin untuk menyelesaikan form ini? setelah anda klik simpan maka data tidak dapat di rubah lagi
							</div>
							<div class='modal-footer'>
								<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
								<button type='submit' class='btn btn-primary' value='save' name='save'>Simpan</button>
							</div>
						</div>
					</div>
				</div>
				<div class='modal fade' id='kembali' tabindex='-1' aria-labelledby='kembaliLabel' aria-hidden='true'>
					<div class='modal-dialog'>
						<div class='modal-content'>
							<div class='modal-header'>
								<h5 class='modal-title' id='kembaliLabel'>Peringatan!!</h5>
								<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
							</div>
							<div class='modal-body'>
								Apakah anda yakin untuk kembali ke halaman sebelumnya? progress anda di halaman ini tidak akan tersimpan
							</div>
							<div class='modal-footer'>
								<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
								<a class="btn btn-primary" href="../perawatan-lebar-sepur/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>&id=<?php echo $id ?>" role="button">Halaman Sebelumnya</a>
							</div>
						</div>
					</div>
				</div>
		</form>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>