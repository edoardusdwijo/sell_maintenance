<?php
include("../../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../../login");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/triwulan-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Status Reporting - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../../"><img src="../../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../../data-aset/"><img src="../../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../../arsip/"><img src="../../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../" class="active"><img src="../../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../../about/"><img src="../../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../../logout.php"><img src="../../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul">
			<p>Status Reporting</p>
		</div>

		<div class="row row-cols-1 row-cols-md-2 g-4 statusReporting mb-4">
			<?php
			$sqlData = "SELECT year(tanggal) as tahun FROM tbl_pemeriksaan WHERE `status`='selesai'";
			$query = mysqli_query($db, $sqlData);
			$tempYear = "";
			while ($data = mysqli_fetch_array($query)) {
				if ($tempYear != $data['tahun']) {
			?>
					<div class="col">
						<div class="card">
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan1">
								<img src="../../../src/image/card-crop.jpg" class="card-img-top" alt="...">
							</a>
							<?php
							$sqlJmlhMax = "SELECT * FROM tbl_data_aset WHERE jenisJalur='Raya'";
							$queryJmlhMax = mysqli_query($db, $sqlJmlhMax);
							$jmlhMax = mysqli_num_rows($queryJmlhMax);
							$progress = 0;
							if ($jmlhMax > 0) {
								while ($dataWesel = mysqli_fetch_array($queryJmlhMax)) {
									$sqlCek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel = '" . $dataWesel['noWesel'] . "' AND month(tanggal)>0 AND month(tanggal)<4 AND year(tanggal)=" . $data['tahun'] . "";
									$queryCek = mysqli_query($db, $sqlCek);
									$dataCek = mysqli_fetch_array($queryCek);
									if (mysqli_num_rows($queryCek) > 0) {
										if ($dataCek['status'] == "selesai") {
											$progress++;
										}
									}
								}
								$persentase = (($progress / $jmlhMax) * 100);
							} else if ($jmlhMax == 0) {
								$persentase = 0;
							}
							?>
							<div class="progress">
								<div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example" style="width: <?php echo $persentase ?>% /*ubah width buat ubah presentase progres*/" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="<?php echo $jmlhMax ?>"></div>
							</div>
							<div class="progres">
								<p><?php echo $persentase ?>% Complete</p>
							</div>
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan1">
								<div class="card-body">
									<h5 class="card-title text-center">Triwulan I Th.<?php echo $data['tahun'] ?></h5>
								</div>
							</a>
						</div>
					</div>
					<div class="col">
						<div class="card">
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan2">
								<img src="../../../src/image/card-crop.jpg" class="card-img-top" alt="...">
							</a>
							<?php
							$sqlJmlhMax = "SELECT * FROM tbl_data_aset WHERE jenisJalur='Raya'";
							$queryJmlhMax = mysqli_query($db, $sqlJmlhMax);
							$jmlhMax = mysqli_num_rows($queryJmlhMax);
							$progress = 0;
							if ($jmlhMax > 0) {
								while ($dataWesel = mysqli_fetch_array($queryJmlhMax)) {
									$sqlCek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel = '" . $dataWesel['noWesel'] . "' AND month(tanggal)>3 AND month(tanggal)<7 AND year(tanggal)=" . $data['tahun'] . "";
									$queryCek = mysqli_query($db, $sqlCek);
									$dataCek = mysqli_fetch_array($queryCek);
									if (mysqli_num_rows($queryCek) > 0) {
										if ($dataCek['status'] == "selesai") {
											$progress++;
										}
									}
								}
								$persentase = (($progress / $jmlhMax) * 100);
							} else if ($jmlhMax == 0) {
								$persentase = 0;
							}
							?>
							<div class="progress">
								<div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example" style="width: <?php echo $persentase ?>% /*ubah width buat ubah presentase progres*/" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="<?php echo $jmlhMax ?>"></div>
							</div>
							<div class="progres">
								<p><?php echo $persentase ?>% Complete</p>
							</div>
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan2">
								<div class="card-body">
									<h5 class="card-title text-center">Triwulan II Th.<?php echo $data['tahun'] ?></h5>
								</div>
							</a>
						</div>
					</div>
					<div class="col">
						<div class="card">
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan3">
								<img src="../../../src/image/card-crop.jpg" class="card-img-top" alt="...">
							</a>
							<?php
							$sqlJmlhMax = "SELECT * FROM tbl_data_aset WHERE jenisJalur='Raya'";
							$queryJmlhMax = mysqli_query($db, $sqlJmlhMax);
							$jmlhMax = mysqli_num_rows($queryJmlhMax);
							$progress = 0;
							if ($jmlhMax > 0) {
								while ($dataWesel = mysqli_fetch_array($queryJmlhMax)) {
									$sqlCek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel = '" . $dataWesel['noWesel'] . "' AND month(tanggal)>6 AND month(tanggal)<10 AND year(tanggal)=" . $data['tahun'] . "";
									$queryCek = mysqli_query($db, $sqlCek);
									$dataCek = mysqli_fetch_array($queryCek);
									if (mysqli_num_rows($queryCek) > 0) {
										if ($dataCek['status'] == "selesai") {
											$progress++;
										}
									}
								}
								$persentase = (($progress / $jmlhMax) * 100);
							} else if ($jmlhMax == 0) {
								$persentase = 0;
							}
							?>
							<div class="progress">
								<div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example" style="width: <?php echo $persentase ?>% /*ubah width buat ubah presentase progres*/" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="<?php echo $jmlhMax ?>"></div>
							</div>
							<div class="progres">
								<p><?php echo $persentase ?>% Complete</p>
							</div>
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan3">
								<div class="card-body">
									<h5 class="card-title text-center">Triwulan III Th.<?php echo $data['tahun'] ?></h5>
								</div>
							</a>
						</div>
					</div>
					<div class="col">
						<div class="card">
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan4">
								<img src="../../../src/image/card-crop.jpg" class="card-img-top" alt="...">
							</a>
							<?php
							$sqlJmlhMax = "SELECT * FROM tbl_data_aset WHERE jenisJalur='Raya'";
							$queryJmlhMax = mysqli_query($db, $sqlJmlhMax);
							$jmlhMax = mysqli_num_rows($queryJmlhMax);
							$progress = 0;
							if ($jmlhMax > 0) {
								while ($dataWesel = mysqli_fetch_array($queryJmlhMax)) {
									$sqlCek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel = '" . $dataWesel['noWesel'] . "' AND month(tanggal)>9 AND month(tanggal)<13 AND year(tanggal)=" . $data['tahun'] . "";
									$queryCek = mysqli_query($db, $sqlCek);
									$dataCek = mysqli_fetch_array($queryCek);
									if (mysqli_num_rows($queryCek) > 0) {
										if ($dataCek['status'] == "selesai") {
											$progress++;
										}
									}
								}
								$persentase = (($progress / $jmlhMax) * 100);
							} else if ($jmlhMax == 0) {
								$persentase = 0;
							}
							?>
							<div class="progress">
								<div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example" style="width: <?php echo $persentase ?>% /*ubah width buat ubah presentase progres*/" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="<?php echo $jmlhMax ?>"></div>
							</div>
							<div class="progres">
								<p><?php echo $persentase ?>% Complete</p>
							</div>
							<a href="../detail-wesel/?jalur=Raya&tahun=<?php echo $data['tahun'] ?>&periode=triwulan4">
								<div class="card-body">
									<h5 class="card-title text-center">Triwulan IV Th.<?php echo $data['tahun'] ?></h5>
								</div>
							</a>
						</div>
					</div>
			<?php
				}
				$tempYear = $data['tahun'];
			}
			?>
		</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>