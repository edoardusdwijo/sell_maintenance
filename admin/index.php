<?php
include("../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../login");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="preload" as="image" href="../src/img/<?php echo $data['strukturOrg'] ?>">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../css/home-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Home - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../src/image/kaiLogo.png" alt="">
		</div>
		<a class="active" href="./"><img src="../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="data-aset/"><img src="../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="arsip/"><img src="../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="status-reporting/"><img src="../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="about/"><img src="../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="logout.php"><img src="../src/icon/icon-logout.png" alt="" class="icon">Logout</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="header text-center">
			<p>Jalan Rel dan Jembatan<br>DAOP 5 Purwokerto</p>
		</div>
		<div class="struktur text-center">
			<?php
			$query = "SELECT * FROM home WHERE id=1";
			$sql = mysqli_query($db, $query);

			if ($data = mysqli_fetch_array($sql)) {
			?> <img src="../src/img/<?php echo $data['strukturOrg'] ?>" alt=""> <?php
																			}
																				?>
		</div>
		<div class="deskripsi">
			<div class="row">
				<div class="col-sm-6">
					<div class="card">
						<h5 class="card-header text-center">Deskripsi Unit JJ</h5>
						<div class="card-body">
							<p class="card-text text-center">
								<?php echo $data['deskripsi'] ?>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 scroll">
					<div class="card">
						<h5 class="card-header text-center">Daftar Resor Unit JJ DAOP 5 Purwokerto</h5>
						<div class="card-body">
							<?php
							$sql = "SELECT * FROM tbl_resort";
							$query = mysqli_query($db, $sql);
							$no = 1;

							while ($data = mysqli_fetch_array($query)){
								?><center><p class="card-text"><?php echo $no.". ".$data['namaResort']; ?></p></center><?php
								$no++;
							}

							?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>