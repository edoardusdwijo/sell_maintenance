<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
    header("location:../../login");
    exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if(isset($_POST['next'])){
    $id = $_POST['id'];
    $ls_ls6m_jarumLurus_nPerawatan = $_POST['ls_ls6m_jarumLurus_nPerawatan'];
    $ls_ls6m_jarumBelok_nPerawatan = $_POST['ls_ls6m_jarumBelok_nPerawatan'];
    $ls_lst_jarumLurus_nPerawatan = $_POST['ls_lst_jarumLurus_nPerawatan'];
    $ls_lst_jarumBelok_nPerawatan = $_POST['ls_lst_jarumBelok_nPerawatan'];
    $ls_la_paksaLurus_nPerawatan1 = $_POST['ls_la_paksaLurus_nPerawatan1'];
    $ls_la_paksaLurus_nPerawatan2 = $_POST['ls_la_paksaLurus_nPerawatan2'];
    $ls_la_paksaLurus_nPerawatan3 = $_POST['ls_la_paksaLurus_nPerawatan3'];
    $ls_la_paksaLurus_nPerawatan4 = $_POST['ls_la_paksaLurus_nPerawatan4'];
    $ls_la_paksaBelok_nPerawatan1 = $_POST['ls_la_paksaBelok_nPerawatan1'];
    $ls_la_paksaBelok_nPerawatan2 = $_POST['ls_la_paksaBelok_nPerawatan2'];
    $ls_la_paksaBelok_nPerawatan3 = $_POST['ls_la_paksaBelok_nPerawatan3'];
    $ls_la_paksaBelok_nPerawatan4 = $_POST['ls_la_paksaBelok_nPerawatan4'];
    $ls_ls_tMatematisLurus_nPerawatan = $_POST['ls_ls_tMatematisLurus_nPerawatan'];
    $ls_ls_tMatematisBelok_nPerawatan = $_POST['ls_ls_tMatematisBelok_nPerawatan'];
    $ls_ls_ppLidahLurus_nPerawatan = $_POST['ls_ls_ppLidahLurus_nPerawatan'];
    $ls_ls_ppLidahBelok_nPerawatan = $_POST['ls_ls_ppLidahBelok_nPerawatan'];
    $ls_ls_pLidahLurus_nPerawatan = $_POST['ls_ls_pLidahLurus_nPerawatan'];
    $ls_ls_pLidahBelok_nPerawatan = $_POST['ls_ls_pLidahBelok_nPerawatan'];
    $ls_ls_lantak_nPerawatan = $_POST['ls_ls_lantak_nPerawatan'];

    $query = "UPDATE tbl_pemeriksaan_ls SET ls_ls6m_jarumLurus_nPerawatan = '$ls_ls6m_jarumLurus_nPerawatan',
        ls_ls6m_jarumBelok_nPerawatan = '$ls_ls6m_jarumBelok_nPerawatan',
        ls_lst_jarumLurus_nPerawatan = '$ls_lst_jarumLurus_nPerawatan',
        ls_lst_jarumBelok_nPerawatan = '$ls_lst_jarumBelok_nPerawatan',
        ls_la_paksaLurus_nPerawatan1 = '$ls_la_paksaLurus_nPerawatan1',
        ls_la_paksaLurus_nPerawatan2 = '$ls_la_paksaLurus_nPerawatan2',
        ls_la_paksaLurus_nPerawatan3 = '$ls_la_paksaLurus_nPerawatan3',
        ls_la_paksaLurus_nPerawatan4 = '$ls_la_paksaLurus_nPerawatan4',
        ls_la_paksaBelok_nPerawatan1 = '$ls_la_paksaBelok_nPerawatan1',
        ls_la_paksaBelok_nPerawatan2 = '$ls_la_paksaBelok_nPerawatan2',
        ls_la_paksaBelok_nPerawatan3 = '$ls_la_paksaBelok_nPerawatan3',
        ls_la_paksaBelok_nPerawatan4 = '$ls_la_paksaBelok_nPerawatan4',
        ls_ls_tMatematisLurus_nPerawatan = '$ls_ls_tMatematisLurus_nPerawatan',
        ls_ls_tMatematisBelok_nPerawatan = '$ls_ls_tMatematisBelok_nPerawatan',
        ls_ls_ppLidahLurus_nPerawatan = '$ls_ls_ppLidahLurus_nPerawatan',
        ls_ls_ppLidahBelok_nPerawatan = '$ls_ls_ppLidahBelok_nPerawatan',
        ls_ls_pLidahLurus_nPerawatan = '$ls_ls_pLidahLurus_nPerawatan',
        ls_ls_pLidahBelok_nPerawatan = '$ls_ls_pLidahBelok_nPerawatan',
        ls_ls_lantak_nPerawatan = '$ls_ls_lantak_nPerawatan' WHERE id='$id'
    ";
    $sql = mysqli_query($db, $query);
    if ($sql) {
        echo "
	        <script>
	            alert('DATA BERHASIL DI SIMPAN, KLIK OK UNTUK MELANJUTKAN KE HALAMAN SELANJUTNYA');
				document.location.href = '../perawatan-opname/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."&id=".$id."';
	        </script>
	        ";
    } else {
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../perawatan-lebar-sepur/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."&id=".$id."';
	        </script>
	        ";
    }
}