<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
    header("location:../../login");
    exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (isset($_POST['next'])) {
    $id = $_POST['id'];
    $ukt_laj_paksaLurus_nPerawatan = $_POST['ukt_laj_paksaLurus_nPerawatan'];
    $ukt_laj_paksaBelok_nPerawatan = $_POST['ukt_laj_paksaBelok_nPerawatan'];
    $ukt_la_paksaLurus_nPerawatan = $_POST['ukt_la_paksaLurus_nPerawatan'];
    $ukt_la_paksaBelok_nPerawatan = $_POST['ukt_la_paksaBelok_nPerawatan'];
    $ukt_japl_lantakLurus_nPerawatan = $_POST['ukt_japl_lantakLurus_nPerawatan'];
    $ukt_japl_lantakBelok_nPerawatan = $_POST['ukt_japl_lantakBelok_nPerawatan'];
    $ukt_jault_lantakLurus_nPerawatan = $_POST['ukt_jault_lantakLurus_nPerawatan'];
    $ukt_jault_lantakBelok_nPerawatan = $_POST['ukt_jault_lantakBelok_nPerawatan'];
    $ukt_tss_lantak_nPerawatan = $_POST['ukt_tss_lantak_nPerawatan'];
    $ukt_connectingRod_nPerawatan = $_POST['ukt_connectingRod_nPerawatan'];

    $query = "UPDATE tbl_pemeriksaan_ukt SET ukt_laj_paksaLurus_nPerawatan = '$ukt_laj_paksaLurus_nPerawatan',
        ukt_laj_paksaBelok_nPerawatan = '$ukt_laj_paksaBelok_nPerawatan',
        ukt_la_paksaLurus_nPerawatan = '$ukt_la_paksaLurus_nPerawatan',
        ukt_la_paksaBelok_nPerawatan = '$ukt_la_paksaBelok_nPerawatan',
        ukt_japl_lantakLurus_nPerawatan = '$ukt_japl_lantakLurus_nPerawatan',
        ukt_japl_lantakBelok_nPerawatan = '$ukt_japl_lantakBelok_nPerawatan',
        ukt_jault_lantakLurus_nPerawatan = '$ukt_jault_lantakLurus_nPerawatan',
        ukt_jault_lantakBelok_nPerawatan = '$ukt_jault_lantakBelok_nPerawatan',
        ukt_tss_lantak_nPerawatan = '$ukt_tss_lantak_nPerawatan',
        ukt_connectingRod_nPerawatan = '$ukt_connectingRod_nPerawatan' WHERE id = '$id'
    ";
    $sql = mysqli_query($db, $query);

    $query2 = "UPDATE tbl_pemeriksaan SET `status` = 'sedangPerawatan' WHERE id='$id'";
    $sql2 = mysqli_query($db, $query2);

    if ($sql && $sql2) {
        echo "
	        <script>
	            alert('DATA BERHASIL DI SIMPAN, KLIK OK UNTUK MELANJUTKAN KE HALAMAN SELANJUTNYA');
				document.location.href = '../perawatan-lebar-sepur/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."&id=".$id."';
	        </script>
	        ";
    } else {
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../perawatan/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
	        </script>
	        ";
    }
}
