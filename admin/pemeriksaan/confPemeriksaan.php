<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
    header("location:../../login");
    exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if ($_SESSION['id' . $noWesel] == "") {
    $id = base_convert(microtime(false), 10, 36);
}

if (isset($_POST['next'])) {
    $_SESSION['id' . $noWesel] = $id;
    $idtemp = $_SESSION['id' . $noWesel];
    $tanggal = $_POST['tanggal'];
    $ukt_laj_paksaLurus_nStandar = $_POST['ukt_laj_paksaLurus_nStandar'];
    $ukt_laj_paksaLurus_nPemeriksaan = $_POST['ukt_laj_paksaLurus_nPemeriksaan'];
    $ukt_laj_paksaBelok_nStandar = $_POST['ukt_laj_paksaBelok_nStandar'];
    $ukt_laj_paksaBelok_nPemeriksaan = $_POST['ukt_laj_paksaBelok_nPemeriksaan'];
    $ukt_la_paksaLurus_nStandar = $_POST['ukt_la_paksaLurus_nStandar'];
    $ukt_la_paksaLurus_nPemeriksaan = $_POST['ukt_la_paksaLurus_nPemeriksaan'];
    $ukt_la_paksaBelok_nStandar = $_POST['ukt_la_paksaBelok_nStandar'];
    $ukt_la_paksaBelok_nPemeriksaan = $_POST['ukt_la_paksaBelok_nPemeriksaan'];
    $ukt_japl_lantakLurus_nStandar = $_POST['ukt_japl_lantakLurus_nStandar'];
    $ukt_japl_lantakLurus_nPemeriksaan = $_POST['ukt_japl_lantakLurus_nPemeriksaan'];
    $ukt_japl_lantakBelok_nStandar = $_POST['ukt_japl_lantakBelok_nStandar'];
    $ukt_japl_lantakBelok_nPemeriksaan = $_POST['ukt_japl_lantakBelok_nPemeriksaan'];
    $ukt_jault_lantakLurus_nStandar = $_POST['ukt_jault_lantakLurus_nStandar'];
    $ukt_jault_lantakLurus_nPemeriksaan = $_POST['ukt_jault_lantakLurus_nPemeriksaan'];
    $ukt_jault_lantakBelok_nStandar = $_POST['ukt_jault_lantakBelok_nStandar'];
    $ukt_jault_lantakBelok_nPemeriksaan = $_POST['ukt_jault_lantakBelok_nPemeriksaan'];
    $ukt_tss_lantak_nStandar = $_POST['ukt_tss_lantak_nStandar'];
    $ukt_tss_lantak_nPemeriksaan = $_POST['ukt_tss_lantak_nPemeriksaan'];
    $ukt_connectingRod_nStandar = $_POST['ukt_connectingRod_nStandar'];
    $ukt_connectingRod_nPemeriksaan = $_POST['ukt_connectingRod_nPemeriksaan'];

    $query = "INSERT INTO tbl_pemeriksaan (id, tanggal, resort, emplasemen, noWesel) VALUE ('$idtemp', '$tanggal', '$resort', '$emplasemen', '$noWesel')";
    $sql = mysqli_query($db, $query);

    if ($sql) {
        $queryUkt = "INSERT INTO tbl_pemeriksaan_ukt (id, ukt_laj_paksaLurus_nStandar, 
        ukt_laj_paksaLurus_nPemeriksaan, 
        ukt_laj_paksaBelok_nStandar, 
        ukt_laj_paksaBelok_nPemeriksaan, 
        ukt_la_paksaLurus_nStandar, 
        ukt_la_paksaLurus_nPemeriksaan, 
        ukt_la_paksaBelok_nStandar, 
        ukt_la_paksaBelok_nPemeriksaan, 
        ukt_japl_lantakLurus_nStandar, 
        ukt_japl_lantakLurus_nPemeriksaan, 
        ukt_japl_lantakBelok_nStandar, 
        ukt_japl_lantakBelok_nPemeriksaan, 
        ukt_jault_lantakLurus_nStandar, 
        ukt_jault_lantakLurus_nPemeriksaan,	
        ukt_jault_lantakBelok_nStandar,
        ukt_jault_lantakBelok_nPemeriksaan,
        ukt_tss_lantak_nStandar,
        ukt_tss_lantak_nPemeriksaan,
        ukt_connectingRod_nStandar,
        ukt_connectingRod_nPemeriksaan) VALUE ('$idtemp',
        '$ukt_laj_paksaLurus_nStandar',
        '$ukt_laj_paksaLurus_nPemeriksaan',
        '$ukt_laj_paksaBelok_nStandar',
        '$ukt_laj_paksaBelok_nPemeriksaan',
        '$ukt_la_paksaLurus_nStandar',
        '$ukt_la_paksaLurus_nPemeriksaan',
        '$ukt_la_paksaBelok_nStandar',
        '$ukt_la_paksaBelok_nPemeriksaan',
        '$ukt_japl_lantakLurus_nStandar',
        '$ukt_japl_lantakLurus_nPemeriksaan',
        '$ukt_japl_lantakBelok_nStandar',
        '$ukt_japl_lantakBelok_nPemeriksaan',
        '$ukt_jault_lantakLurus_nStandar',
        '$ukt_jault_lantakLurus_nPemeriksaan',
        '$ukt_jault_lantakBelok_nStandar',
        '$ukt_jault_lantakBelok_nPemeriksaan',
        '$ukt_tss_lantak_nStandar',
        '$ukt_tss_lantak_nPemeriksaan',
        '$ukt_connectingRod_nStandar',
        '$ukt_connectingRod_nPemeriksaan')";
        $sqlUkt = mysqli_query($db, $queryUkt);
        if ($sqlUkt) {
            echo "
	        <script>
	            alert('DATA BERHASIL DI SIMPAN, KLIK OK UNTUK MELANJUTKAN KE HALAMAN SELANJUTNYA');
				document.location.href = '../pemeriksaan-lebar-sepur/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
	        </script>
	        ";
        } else {
            echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../pemeriksaan';
	        </script>
	        ";
        }
    } else {
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../pemeriksaan';
	        </script>
	    ";
    }
}

if (isset($_POST['edit'])) {
    $id = $_SESSION['id' . $noWesel];
    $ukt_laj_paksaLurus_nStandar = $_POST['ukt_laj_paksaLurus_nStandar'];
    $ukt_laj_paksaLurus_nPemeriksaan = $_POST['ukt_laj_paksaLurus_nPemeriksaan'];
    $ukt_laj_paksaBelok_nStandar = $_POST['ukt_laj_paksaBelok_nStandar'];
    $ukt_laj_paksaBelok_nPemeriksaan = $_POST['ukt_laj_paksaBelok_nPemeriksaan'];
    $ukt_la_paksaLurus_nStandar = $_POST['ukt_la_paksaLurus_nStandar'];
    $ukt_la_paksaLurus_nPemeriksaan = $_POST['ukt_la_paksaLurus_nPemeriksaan'];
    $ukt_la_paksaBelok_nStandar = $_POST['ukt_la_paksaBelok_nStandar'];
    $ukt_la_paksaBelok_nPemeriksaan = $_POST['ukt_la_paksaBelok_nPemeriksaan'];
    $ukt_japl_lantakLurus_nStandar = $_POST['ukt_japl_lantakLurus_nStandar'];
    $ukt_japl_lantakLurus_nPemeriksaan = $_POST['ukt_japl_lantakLurus_nPemeriksaan'];
    $ukt_japl_lantakBelok_nStandar = $_POST['ukt_japl_lantakBelok_nStandar'];
    $ukt_japl_lantakBelok_nPemeriksaan = $_POST['ukt_japl_lantakBelok_nPemeriksaan'];
    $ukt_jault_lantakLurus_nStandar = $_POST['ukt_jault_lantakLurus_nStandar'];
    $ukt_jault_lantakLurus_nPemeriksaan = $_POST['ukt_jault_lantakLurus_nPemeriksaan'];
    $ukt_jault_lantakBelok_nStandar = $_POST['ukt_jault_lantakBelok_nStandar'];
    $ukt_jault_lantakBelok_nPemeriksaan = $_POST['ukt_jault_lantakBelok_nPemeriksaan'];
    $ukt_tss_lantak_nStandar = $_POST['ukt_tss_lantak_nStandar'];
    $ukt_tss_lantak_nPemeriksaan = $_POST['ukt_tss_lantak_nPemeriksaan'];
    $ukt_connectingRod_nStandar = $_POST['ukt_connectingRod_nStandar'];
    $ukt_connectingRod_nPemeriksaan = $_POST['ukt_connectingRod_nPemeriksaan'];

    $queryUkt = "UPDATE tbl_pemeriksaan_ukt SET
        ukt_laj_paksaLurus_nStandar='$ukt_laj_paksaLurus_nStandar', 
        ukt_laj_paksaLurus_nPemeriksaan='$ukt_laj_paksaLurus_nPemeriksaan', 
        ukt_laj_paksaBelok_nStandar='$ukt_laj_paksaBelok_nStandar', 
        ukt_laj_paksaBelok_nPemeriksaan='$ukt_laj_paksaBelok_nPemeriksaan', 
        ukt_la_paksaLurus_nStandar='$ukt_la_paksaLurus_nStandar', 
        ukt_la_paksaLurus_nPemeriksaan='$ukt_la_paksaLurus_nPemeriksaan', 
        ukt_la_paksaBelok_nStandar='$ukt_la_paksaBelok_nStandar', 
        ukt_la_paksaBelok_nPemeriksaan='$ukt_la_paksaBelok_nPemeriksaan', 
        ukt_japl_lantakLurus_nStandar='$ukt_japl_lantakLurus_nStandar', 
        ukt_japl_lantakLurus_nPemeriksaan='$ukt_japl_lantakLurus_nPemeriksaan', 
        ukt_japl_lantakBelok_nStandar='$ukt_japl_lantakBelok_nStandar', 
        ukt_japl_lantakBelok_nPemeriksaan='$ukt_japl_lantakBelok_nPemeriksaan', 
        ukt_jault_lantakLurus_nStandar='$ukt_jault_lantakLurus_nStandar', 
        ukt_jault_lantakLurus_nPemeriksaan='$ukt_jault_lantakLurus_nPemeriksaan',	
        ukt_jault_lantakBelok_nStandar='$ukt_jault_lantakBelok_nStandar',
        ukt_jault_lantakBelok_nPemeriksaan='$ukt_jault_lantakBelok_nPemeriksaan',
        ukt_tss_lantak_nStandar='$ukt_tss_lantak_nStandar',
        ukt_tss_lantak_nPemeriksaan='$ukt_tss_lantak_nPemeriksaan',
        ukt_connectingRod_nStandar='$ukt_connectingRod_nStandar',
        ukt_connectingRod_nPemeriksaan='$ukt_connectingRod_nPemeriksaan' WHERE id='$id'";
    $sqlUkt = mysqli_query($db, $queryUkt);
    if ($sqlUkt) {
        echo "
	        <script>
	            alert('DATA BERHASIL DI SIMPAN, KLIK OK UNTUK MELANJUTKAN KE HALAMAN SELANJUTNYA');
				document.location.href = '../pemeriksaan-lebar-sepur/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
	        </script>
	        ";
    } else {
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../pemeriksaan';
	        </script>
	        ";
    }
}
