<?php
include("../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
	header("location:../login");
	exit;
}

if (isset($_POST['save'])) {
    $nama = $_POST['nama'];
    $nipp = $_POST['nipp'];
    $kodeResort = $_POST['kodeResort'];
    $jabatan = $_POST['jabatan'];
    $no_hp = $_POST['no_hp'];
    $email = $_POST['email'];
    $role = $_POST['role'];
    $password = md5($_POST['password']);

    $query = "INSERT INTO tbl_user (nipp, nama, kodeResort, jabatan, no_hp, email, `role`, `password`) VALUE ('$nipp', '$nama', '$kodeResort', '$jabatan', '$no_hp', '$email', '$role', '$password')";
    $sql = mysqli_query($db, $query);

    if($sql){
        echo "
	        <script>
	            alert('DATA BARU BERHASIL DI INPUT');
				document.location.href = '../super_admin';
	        </script>
	    ";
    } else {
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../super_admin';
	        </script>
	    ";
    }

}

if (isset($_POST['edit'])) {
    $nama = $_POST['nama'];
    $nipp = $_POST['nipp'];
    $kodeResort = $_POST['kodeResort'];
    $jabatan = $_POST['jabatan'];
    $no_hp = $_POST['no_hp'];
    $email = $_POST['email'];
    $role = $_POST['role'];
    $password = $_POST['password'];
    $passEncrypt = md5($password);

    if ($password == ""){
        $query = "UPDATE tbl_user SET nama='$nama', kodeResort='$kodeResort', jabatan='$jabatan', no_hp='$no_hp', email='$email', `role`='$role' WHERE nipp=$nipp";
        $sql = mysqli_query($db, $query);

        if($sql){
            echo "
                <script>
                    alert('DATA BERHASIL DI EDIT');
                    document.location.href = '../super_admin';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('DATA GAGAL DI EDIT');
                    document.location.href = '../super_admin';
                </script>
            ";
        }
    } else {
        $query = "UPDATE tbl_user SET nama='$nama', kodeResort='$kodeResort', jabatan='$jabatan', no_hp='$no_hp', email='$email', `role`='$role', `password`='$passEncrypt' WHERE nipp=$nipp";
        $sql = mysqli_query($db, $query);

        if($sql){
            echo "
                <script>
                    alert('DATA BERHASIL DI EDIT');
                    document.location.href = '../super_admin';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('DATA GAGAL DI EDIT');
                    document.location.href = '../super_admin';
                </script>
            ";
        }
    }

}

if (isset($_POST['hapus'])) {
    $nipp = $_POST['nipp'];

    $query = "DELETE FROM tbl_user WHERE nipp='$nipp'";
    $sql = mysqli_query($db, $query);

    if ($sql){
        echo "
            <script>
                alert('DATA BERHASIL DI HAPUS');
                document.location.href = '../super_admin';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('DATA GAGAL DI HAPUS');
                document.location.href = '../super_admin';
            </script>
        ";
    }
}
?>