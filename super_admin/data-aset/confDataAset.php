<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
    header("location:../login");
    exit;
}

if (isset($_POST['save'])) {
    $noWesel = $_POST['noWesel'];
    $emplasemen = $_POST['emplasemen'];
    $posisiUjung = $_POST['posisiUjung'];
    $posisiPangkal = $_POST['posisiPangkal'];
    $sudutWesel = $_POST['sudutWesel'];
    $merk = $_POST['merk'];
    $arah = $_POST['arah'];
    $tipeRel = $_POST['tipeRel'];
    $lidah = $_POST['lidah'];
    $terlayan = $_POST['terlayan'];
    $tahun = $_POST['tahun'];
    $jenisJalur = $_POST['jenisJalur'];

    $fotoWeselNama = $_FILES['fotoWesel']['name'];
    $fotoWeselTmp = $_FILES['fotoWesel']['tmp_name'];
    $fotoWeselNamaBaru = $noWesel . $fotoWeselNama;
    $fotoWeselSize = $_FILES['fotoWesel']['size'];
    $fotoWeselTipe = $_FILES['fotoWesel']['type'];

    if ($fotoWeselTipe == "image/jpeg" || $fotoWeselTipe == "image/png") {
        if ($fotoWeselSize <= 5000000) {
            if (move_uploaded_file($fotoWeselTmp, "../../src/img/" . $fotoWeselNamaBaru)) {
                $query = "INSERT INTO tbl_data_aset (noWesel, emplasemen, posisiUjung, posisiPangkal, sudutWesel, merk, arah, tipeRel, lidah, terlayan, tahun, jenisJalur, fotoWesel) VALUE ('$noWesel','$emplasemen', '$posisiUjung', '$posisiPangkal', '$sudutWesel', '$merk', '$arah', '$tipeRel', '$lidah', '$terlayan', '$tahun' , '$jenisJalur', '$fotoWeselNamaBaru')";
                $sql = mysqli_query($db, $query);

                if ($sql) {
                    echo "
                        <script>
                            alert('DATA BARU BERHASIL DI INPUT');
                            document.location.href = '../data-aset';
                        </script>
                    ";
                } else {
                    echo "
                        <script>
                            alert('DATA GAGAL DI INPUT');
                            document.location.href = '../data-aset';
                        </script>
                    ";
                }
            }
        } else {
            echo "
                    <script>
                        alert('DATA GAGAL DI INPUT, GAMBAR TIDAK BOLEH LEBIH DARI 5MB');
                        document.location.href = '../data-aset';
                    </script>
                ";
        }
    } else {
        echo "
                <script>
                    alert('DATA GAGAL DI INPUT, TYPE FILE YANG ANDA UPLOAD BUKAN GAMBAR');
                    document.location.href = '../data-aset';
                </script>
            ";
    }
}

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $noWesel = $_POST['noWesel'];
    $emplasemen = $_POST['emplasemen'];
    $posisiUjung = $_POST['posisiUjung'];
    $posisiPangkal = $_POST['posisiPangkal'];
    $sudutWesel = $_POST['sudutWesel'];
    $merk = $_POST['merk'];
    $arah = $_POST['arah'];
    $tipeRel = $_POST['tipeRel'];
    $lidah = $_POST['lidah'];
    $terlayan = $_POST['terlayan'];
    $tahun = $_POST['tahun'];
    $jenisJalur = $_POST['jenisJalur'];

    if ($_FILES['fotoWesel']['tmp_name']!='') {
        $fotoWeselNama = $_FILES['fotoWesel']['name'];
        $fotoWeselTmp = $_FILES['fotoWesel']['tmp_name'];
        $fotoWeselNamaBaru = $noWesel . $fotoWeselNama;
        $fotoWeselSize = $_FILES['fotoWesel']['size'];
        $fotoWeselTipe = $_FILES['fotoWesel']['type'];

        if ($fotoWeselTipe == "image/jpeg" || $fotoWeselTipe == "image/png") {
            if ($fotoWeselSize <= 5000000) {
                if (move_uploaded_file($fotoWeselTmp, "../../src/img/" . $fotoWeselNamaBaru)) {
                    $query = "UPDATE tbl_data_aset SET noWesel='$noWesel', emplasemen='$emplasemen', posisiUjung='$posisiUjung', posisiPangkal='$posisiPangkal', sudutWesel='$sudutWesel', merk='$merk', arah='$arah', tipeRel='$tipeRel', lidah='$lidah', terlayan='$terlayan', tahun='$tahun', jenisJalur='$jenisJalur', fotoWesel='$fotoWeselNamaBaru' WHERE id=$id";
                    $sql = mysqli_query($db, $query);

                    if ($sql) {
                        echo "
                            <script>
                                alert('DATA BERHASIL DI EDIT');
                                document.location.href = '../data-aset';
                            </script>
                        ";
                    } else {
                        echo "
                            <script>
                                alert('DATA GAGAL DI EDIT');
                                document.location.href = '../data-aset';
                            </script>
                        ";
                    }
                }
            } else {
                echo "
                        <script>
                            alert('DATA GAGAL DI INPUT, GAMBAR TIDAK BOLEH LEBIH DARI 5MB');
                            document.location.href = '../data-aset';
                        </script>
                    ";
            }
        } else {
            echo "
                    <script>
                        alert('DATA GAGAL DI INPUT, TYPE FILE YANG ANDA UPLOAD BUKAN GAMBAR');
                        document.location.href = '../data-aset';
                    </script>
                ";
        }
    }else{
        $query = "UPDATE tbl_data_aset SET noWesel='$noWesel', emplasemen='$emplasemen', posisiUjung='$posisiUjung', posisiPangkal='$posisiPangkal', sudutWesel='$sudutWesel', merk='$merk', arah='$arah', tipeRel='$tipeRel', lidah='$lidah', terlayan='$terlayan', tahun='$tahun', jenisJalur='$jenisJalur' WHERE id=$id";
        $sql = mysqli_query($db, $query);

        if ($sql) {
            echo "
                <script>
                    alert('DATA BERHASIL DI EDIT');
                    document.location.href = '../data-aset';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('DATA GAGAL DI EDIT');
                    document.location.href = '../data-aset';
                </script>
            ";
        }
    }
}

if (isset($_POST['hapus'])) {
    $id = $_POST['id'];

    $query = "DELETE FROM tbl_data_aset WHERE id='$id'";
    $sql = mysqli_query($db, $query);

    if ($sql) {
        echo "
            <script>
                alert('DATA BERHASIL DI HAPUS');
                document.location.href = '../data-aset';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('DATA GAGAL DI HAPUS');
                document.location.href = '../data-aset';
            </script>
        ";
    }
}
