<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="preload" as="image" href="src/image/logo.png">
	<link rel="preload" as="image" href="src/image/bg-image.png">
	<!-- <link rel="preload" as="image" href="src/image/hero-real.png"> -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="css/halaman_awal.css">
	<title>Halaman Awal</title>
</head>
<body>
	<!-- start logo -->
	<div class="logo pt-4">
		<div class="container text-left">
		  <div class="row">
		    <div class="col-6 col-md-2">
		    	<div class="gambar">
		    		<img src="src/image/logo.png" alt="">
		    	</div>
		    </div>
		    <div class="col-6 col-md-4">
		    	<div class="kai">
		    		<p>
			    		PT KERETA API INDONESIA (Persero)<br>DAOP 5 PURWOKERTO
			    	</p>
		    	</div>
		    </div>

		    <!-- Force next columns to break to new line -->
		    <div class="w-100"></div>
		  </div>
		</div>
	</div>
	<!-- end logo -->

	<!-- start konten -->
	<div class="container">
      <div class="row justify-content-between">
      	<div class="col-lg-5 order-1 order-lg-1 hero-img" data-aos="zoom-out" data-aos-delay="300">
          <img src="src/image/hero-real.webp" class="img-fluid animated" alt="">
        </div>
        <div class="col-lg-5 pt-5 pt-lg-2 order-2 order-lg-2 d-flex align-items-center">
          <div data-aos="zoom-out">
            <h1 class="judul">Selamat Datang!</h1>
            <p class="pilih">Silahkan Pilih</p>
            <div class="text-center text-lg-start">
            	<div class="tombol">
            		<center>
            			<button class="btn btn-1" type="button">Lengkung</button>
                  <button class="btn btn-2" type="button" onclick="document.location='login/'">Wesel</button>
            		</center>
            	</div>
            	<div class="footer">
            		<p>Jalan Rel dan Jembatan DAOP 5 Purwokerto</p>
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</html>